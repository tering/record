# 项目介绍

这是个记录我各项指标的 demo 项目
项目的目的是练习最近学习的一些技术点

| 技术点     | 用途                       |
| ---------- | -------------------------- |
| golang     | 作为后端服务语言           |
| MongoDB    | 作为数据库                 |
| vue        | 作为前端 js 开发框架       |
| element-ui | vue 的 ui 框架             |
| axios      | 发送 ajax 请求的 js 库     |
| moment     | 作为前端时间格式化的 js 库 |

这里只有 go 的后端项目，[前端项目在这里](../tering/resources/record)

## 安装方式

```shell
go install gitee.com/tering/record
```

## 发布方式

- 项目采用前后端分离的模式
- 前端发布在 gitee 上
- 后端服务发布在阿里云上
- 过段时间，我会学习 dockerfile 的编写，将服务容器化
- [测试页面](https://tering.gitee.io/record)
