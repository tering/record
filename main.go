package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitee.com/tering/record/db"
	"gitee.com/tering/record/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func main() {
	handleFunc("/record", recordPDHandler)
	handleFunc("/record/list", listRecordHandler)
	http.ListenAndServeTLS(":8090", "server.crt", "server.key", nil)
}

func handleFunc(pattern string, handler func(http.ResponseWriter, *http.Request)) {
	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")  // 允许所有来源跨域请求
		w.Header().Add("Access-Control-Allow-Headers", "*") // 允许所有 headers 的跨域请求
		w.Header().Add("Access-Control-Allow-Methods", "*") // 允许所有跨域请求方式

		r.ParseForm()
		bodyToForm(r)
		handler(w, r)
	})
}

func readBody(r *http.Request, body interface{}) {
	if r.ContentLength == 0 {
		return
	}
	bodyByte := make([]byte, r.ContentLength)
	r.Body.Read(bodyByte)
	json.Unmarshal(bodyByte, body)
}

func bodyToForm(r *http.Request) {
	if r.ContentLength == 0 {
		return
	}
	var body bson.M
	readBody(r, &body)
	for k, v := range body {
		if vstr, ok := v.(string); ok {
			r.Form.Add(k, vstr)
			fmt.Println(k, vstr)
		}
	}
}

// 将一个 record 请求，按照约定（post、delete）分发到不同的业务处理函数
func recordPDHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		// 根据请求参数中是否包含 _id 判定，是新增还是删除
		if id := r.FormValue("_id"); id == "" {
			insertRecordHandler(w, r)
		} else {
			updateRecordHandler(w, r)
		}
	} else if r.Method == "DELETE" {
		deleteRecordHandler(w, r)
	} else {
		fmt.Fprintln(w, "无效请求")
	}
}

// 插入一个 record 的 handler
func insertRecordHandler(w http.ResponseWriter, r *http.Request) {
	now := time.Now().UnixNano() / 1e6
	value := r.FormValue("value")
	id := db.InsertRecord(bson.M{
		"name":  r.FormValue("name"),
		"value": value,
		"time":  now,
		"log": []bson.M{
			bson.M{
				"time":  now,
				"value": value,
			},
		},
	})
	w.Write(model.OKData(id))
}

// 删除一个 record 的 handler
func deleteRecordHandler(w http.ResponseWriter, r *http.Request) {
	id := r.FormValue("_id")
	if id == "" {
		w.WriteHeader(400)
		fmt.Fprintln(w, "缺少请求参数：'_id'")
	} else {
		if db.DeleteRecordByID(id) {
			w.WriteHeader(205)
		} else {
			w.WriteHeader(500)
		}
	}
}

// 修好一个 record 的 handler
func updateRecordHandler(w http.ResponseWriter, r *http.Request) {
	now := time.Now().UnixNano() / 1e6
	value := r.FormValue("value")
	objID, _ := primitive.ObjectIDFromHex(r.FormValue("_id"))
	res, err := db.Record.UpdateOne(
		context.TODO(),
		bson.M{"_id": objID},
		bson.M{
			"$set": bson.M{
				"name":  r.FormValue("name"),
				"value": value,
				"time":  now,
			},
			"$push": bson.M{
				"log": bson.M{
					"time":  now,
					"value": value,
				},
			},
		},
	)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(500)
		w.Write(model.Error("更新失败，请赶紧查错！！！"))
		return
	}
	if res.MatchedCount == 1 { // 当只匹配一条时，响应成功
		w.Write(model.OK())
	} else {
		w.WriteHeader(500)
		w.Write(model.Error("更新失败，请赶紧查错！！！"))
	}
}

// 获取所有记录对象
func listRecordHandler(w http.ResponseWriter, r *http.Request) {
	w.Write(model.OKData(db.ListAllRecord()))
}
