package db

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Record 记录的 mongo 集合
var Record *mongo.Collection

func init() {
	// 连接到MongoDB
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://127.0.0.1:27017"))
	if err != nil {
		log.Fatal(err)
	}
	Record = client.Database("homepage").Collection("record")
}

// InsertRecord 往数据库里插入一条记录，返回插入以后的 id
func InsertRecord(e bson.M) string {
	res, err := Record.InsertOne(context.TODO(), e)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	if insertedID, ok := res.InsertedID.(primitive.ObjectID); ok {
		fmt.Println(insertedID)
		return insertedID.Hex()
	}
	fmt.Println(res.InsertedID)
	return ""
}

// DeleteRecordByID 根据 id 删除某一条记录
func DeleteRecordByID(id string) bool {
	objID, _ := primitive.ObjectIDFromHex(id)
	res, err := Record.DeleteOne(context.TODO(), bson.M{"_id": objID})
	if err != nil {
		fmt.Println(err)
		return false
	}
	return res.DeletedCount == 1
}

// ListAllRecord 从数据库中取出所有的记录，直接查全部
func ListAllRecord() []bson.M {
	cur, err := Record.Find(context.TODO(), bson.M{}, options.Find())
	if err != nil {
		fmt.Println(err)
	}
	allRecord := make([]bson.M, 0)
	// 将查询结果解析为固定格式
	cur.All(context.TODO(), &allRecord)
	if err := cur.Err(); err != nil {
		fmt.Println(err)
	}
	// 关闭查询结果
	cur.Close(context.TODO())
	return allRecord
}
