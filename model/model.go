package model

import "encoding/json"

import "fmt"

// ResResult 用来传递 http 响应的
type ResResult struct {
	Ok         bool
	Msg        string
	ResultData interface{}
}

// OKData 响应 http 成功
func OKData(data interface{}) []byte {
	return toJSONByte(ResResult{
		Ok:         true,
		Msg:        "success",
		ResultData: data,
	})
}

// OKMsg 响应 http 成功
func OKMsg(msg string) []byte {
	return toJSONByte(ResResult{
		Ok:  true,
		Msg: msg,
	})
}

// OK 响应 http 成功
func OK() []byte {
	return toJSONByte(ResResult{
		Ok:  true,
		Msg: "success",
	})
}

// Error 服务器发生错误时响应
func Error(msg string) []byte {
	return toJSONByte(ResResult{
		Ok:  false,
		Msg: msg,
	})
}

// 将响应对象转换为 json 字节
func toJSONByte(r interface{}) []byte {
	data, err := json.Marshal(r)
	if err != nil {
		fmt.Println(err)
	}
	return data
}
